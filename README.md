# db9-gpio-rpi Pullup Fix

A fix for a db9_gpio_rpi driver issue on Raspberry Pi 4 and 400 & potentially other recent Raspberry Pi OS installs. Puts GPIO pins into pullup mode, required by some joysticks. See https://github.com/marqs85/db9_gpio_rpi/issues/8

Use:
chmod pullup.sh +x
add /path/to/pullup.sh to /etc/rc.local to load on boot

NOTE: this should now be fixed in the driver as of 1 Feb 2021, as detailed in the thread linked above. Leaving this repo up in case it's required by anyone running an older version.
