#!/bin/bash
# fix for db9_gpio_rpi driver issue https://github.com/marqs85/db9_gpio_rpi/issues/8
# RPi 4 and 400 need this tweak to speak to db9_gpio_rpi gpio connected controllers as some inputs need explicit pullup
# ensure that you're applying the pullups to the correct pins - this is for a standard deployment of the driver
#
# Use:
# chmod pullup.sh +x
# add /path/to/pullup.sh to /etc/rc.local to load on boot

# Joyport /dev/input/js0
raspi-gpio set 4 ip pu
raspi-gpio set 7 ip pu
raspi-gpio set 8 ip pu
raspi-gpio set 9 ip pu
raspi-gpio set 10 ip pu
raspi-gpio set 11 ip pu
raspi-gpio set 14 ip pu
# Joyport /dev/input/js1 - if we connect a second joystick
raspi-gpio set 15 ip pu
raspi-gpio set 17 ip pu
raspi-gpio set 18 ip pu
raspi-gpio set 22 ip pu
raspi-gpio set 23 ip pu
raspi-gpio set 24 ip pu
raspi-gpio set 25 ip pu
